import { observer } from 'mobx-react-lite'
import React, { useCallback, useContext, useState } from 'react'
import { Context } from '../..'
import { CreateGroupModal } from '../../components/panels/GroupPanel/CreateGroupModal'
import { EditGroupModal } from '../../components/panels/GroupPanel/EditGroupModal'
import PanelProvider from '../../components/panels/PanelProvider'

export const GroupPanel: React.FC = observer(() => {
  const { group } = useContext(Context)

  const [search, setSearch] = useState<number>(0)
  const [createModalShow, setCreateModalShow] = useState<boolean>(false)
  const [editModal, setEditModal] = useState<{ show: boolean; id: number }>({
    show: false,
    id: 0,
  })
  const filteredGroups = useCallback(() => {
    return group.groups.filter((item) =>
      search ? item.number.toString().includes(search.toString()) : true,
    )
  }, [group.groups, search])

  const handleGearClick = (id: number) => {
    setEditModal({ show: true, id })
  }

  const handleDeleteClick = (id: number) => {
    group.deleteGroup(id)
  }

  return (
    <>
      <PanelProvider>
        <PanelProvider.Header>
          <PanelProvider.Title title="Group panel" />
          <PanelProvider.Search search={search} setSearch={setSearch} />
        </PanelProvider.Header>
        <PanelProvider.Container>
          <PanelProvider.CreateItem setShow={setCreateModalShow} />
          {filteredGroups().map((item) => (
            <PanelProvider.Item
              key={item.id}
              item={item}
              handleDeleteClick={handleDeleteClick}
              handleGearClick={handleGearClick}
            />
          ))}
        </PanelProvider.Container>
      </PanelProvider>
      <CreateGroupModal show={createModalShow} setShow={setCreateModalShow} />
      <EditGroupModal
        show={editModal.show}
        setShow={setEditModal}
        id={editModal.id}
      />
    </>
  )
})
