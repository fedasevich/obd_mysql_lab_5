import {
  Column,
  DataType,
  ForeignKey,
  Model,
  Table,
} from "sequelize-typescript";
import { Group } from "src/group/group.model";
import { Lesson } from "src/lesson/lesson.model";
import { Teacher } from "src/teacher/teacher.model";

interface ScheduleCreationAttrs {
  weekDay: number;
  classNumber: number;
  lessonId: number;
  groupId: number;
  teacherId: number;
}
@Table({ tableName: "schedule" })
export class Schedule extends Model<Schedule, ScheduleCreationAttrs> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @Column({
    type: DataType.INTEGER,
    validate: {
      isNumeric: true,
      customValidator(value: number) {
        if (value < 1 || value > 7) {
          throw new Error("The field must contain a number between 1 and 7");
        }
      },
    },
  })
  weekDay: number;

  @Column({
    type: DataType.INTEGER,
    validate: {
      isNumeric: true,
      customValidator(value: number) {
        if (value < 1 || value > 6) {
          throw new Error("The field must contain a number between 1 and 6");
        }
      },
    },
  })
  classNumber: number;
  @Column({ type: DataType.INTEGER })
  @ForeignKey(() => Lesson)
  lessonId: number;
  @Column({ type: DataType.INTEGER })
  @ForeignKey(() => Group)
  groupId: number;
  @Column({ type: DataType.INTEGER })
  @ForeignKey(() => Teacher)
  teacherId: number;
}
