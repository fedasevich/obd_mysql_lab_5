import { Model, Table, Column, DataType, HasMany } from "sequelize-typescript";
import { Teacher } from "src/teacher/teacher.model";


interface DepartmentCreationAttrs {
    name: string;
    phoneNumber:string;
}

@Table({ tableName: 'department' })
export class Department extends Model<Department, DepartmentCreationAttrs> {
    @Column({ type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true })
    id: number;
    @Column({ type: DataType.STRING, unique: true })
    name: string;
    @Column({ type: DataType.STRING })
    phoneNumber: string;
    @HasMany(() => Teacher)
    teachers: Teacher[];
}
