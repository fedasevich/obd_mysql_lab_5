import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { Context } from '../..'
import { CreateClassModalData } from '../../pages/Schedule'
import { ScheduleData } from '../../store/ScheduleStore'
import { ScheduleModal } from './ScheduleModal'

interface CreateScheduleModalProps {
  classNumber: number
  weekDay: number
  show: boolean
  groupId: number
  setShow: React.Dispatch<React.SetStateAction<CreateClassModalData>>
}

export const CreateScheduleModal: React.FC<CreateScheduleModalProps> = observer(
  ({ show, setShow, classNumber, weekDay, groupId }) => {
    const { schedule } = useContext(Context)

    const handleClose = () => {
      setShow((prev) => ({
        ...prev,
        show: false,
      }))
    }

    const handleCommit = (formData: Omit<ScheduleData, 'id'>) => {
      schedule
        .createSchedule({ ...formData, groupId })
        .then(() => handleClose())
    }

    return (
      <ScheduleModal
        classNumber={classNumber}
        weekDay={weekDay}
        groupId={groupId}
        onSubmit={handleCommit}
        show={show}
        onClose={handleClose}
      />
    )
  },
)
