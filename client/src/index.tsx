import React, { createContext } from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

import 'bootstrap/dist/css/bootstrap.min.css';
import DepartmentStore from './store/DepartmentStore';
import LessonStore from './store/LessonStore';
import GroupStore from './store/GroupStore';
import TeacherStore from './store/TeacherStore';
import ScheduleStore from './store/ScheduleStore';

export const Context = createContext<{ department: DepartmentStore, lesson: LessonStore, group: GroupStore, teacher: TeacherStore, schedule: ScheduleStore }>({
  department: new DepartmentStore(),
  lesson: new LessonStore(),
  group: new GroupStore(),
  teacher: new TeacherStore(),
  schedule: new ScheduleStore(),
});

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <Context.Provider value={{
    department: new DepartmentStore(),
    lesson: new LessonStore(),
    group: new GroupStore(),
    teacher: new TeacherStore(),
    schedule: new ScheduleStore(),
  }}>
    <App />
  </Context.Provider>
);
