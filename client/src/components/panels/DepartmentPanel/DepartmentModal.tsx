import { observer } from 'mobx-react-lite'
import React, { useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import { DepartmentData } from '../../../store/DepartmentStore'
import { PanelModalProps } from '../PanelProvider'

export const DepartmentModal: React.FC<PanelModalProps<DepartmentData>> =
  observer(({ show, onClose, onSubmit }) => {
    const [formData, setFormData] = useState<Omit<DepartmentData, 'id'>>({
      name: '',
      phoneNumber: '',
    })

    const handleCommit = () => {
      onSubmit(formData)
    }

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      setFormData((prev) => ({
        ...prev,
        [event.target.name]: event.target.value,
      }))
    }

    return (
      <Modal show={show} onHide={onClose}>
        <Modal.Header closeButton>
          <Modal.Title>Department modal</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="name">Enter new name:</Form.Label>
              <Form.Control
                type="text"
                name="name"
                value={formData.name}
                id="name"
                placeholder=""
                onChange={handleInputChange}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="phoneNumber">
                Enter new phone number:
              </Form.Label>
              <Form.Control
                type="text"
                name="phoneNumber"
                value={formData.phoneNumber}
                id="phoneNumber"
                placeholder=""
                onChange={handleInputChange}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={onClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleCommit}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    )
  })
