import { observer } from 'mobx-react-lite'
import React from 'react'

import SelectGroup from './SelectGroup'

const MainPage: React.FC = observer(() => {
  return (
    <>
      <SelectGroup />
    </>
  )
})

export default MainPage
