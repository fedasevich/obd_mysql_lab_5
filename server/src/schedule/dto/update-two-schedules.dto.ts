import { IsNumber, IsOptional } from "class-validator";
import { ScheduleDto } from "./schedule.dto";
export class UpdateTwoSchedulesDto {
    readonly fromSchedule: ScheduleDto;
    readonly toSchedule: ScheduleDto;
}
