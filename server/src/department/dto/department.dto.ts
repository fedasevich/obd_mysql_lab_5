import { IsString } from "class-validator";
export class DepartmentDto {
    @IsString({ message: "Must be string" })
    readonly name: string;
    @IsString({ message: "Must be string" })
    readonly phoneNumber: string;
}
