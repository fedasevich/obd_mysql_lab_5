import { Model, Table, Column, DataType, HasMany } from "sequelize-typescript";
import { Schedule } from "src/schedule/schedule.model";


interface LessonCreationAttrs {
    name: string;
}

@Table({ tableName: 'lesson' })
export class Lesson extends Model<Lesson, LessonCreationAttrs> {
    @Column({ type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true })
    id: number;
    @Column({ type: DataType.STRING, unique: true })
    name: string;
    @HasMany(() => Schedule)
    schedules:Schedule[];
}
