import { Model, Table, Column, DataType, HasMany } from "sequelize-typescript";

import { Teacher } from "src/teacher/teacher.model";


interface ProfessionCreationAttrs {
    profession: string;
    capacity: number;
}

@Table({ tableName: 'profession' })
export class Profession extends Model<Profession, ProfessionCreationAttrs> {
    @Column({ type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true })
    id: number;
    @Column({ type: DataType.STRING, unique: true })
    profession: string;
    @Column({ type: DataType.INTEGER })
    capacity: number;
    @HasMany(() => Teacher)
    teachers: Teacher[];
}
