import { observer } from 'mobx-react-lite'
import React, { useContext, useEffect, useState } from 'react'
import { Col, Container, Form } from 'react-bootstrap'
import Row from 'react-bootstrap/esm/Row'
import { useParams } from 'react-router-dom'
import { Context } from '..'
import { AddClassButton } from '../components/AddClassButton'
import { DraggableLesson } from '../components/DraggableLesson'
import { LessonItem } from '../components/LessonItem'
import { PlaceholderLessonItem } from '../components/PlaceholderLessonItem'
import { ScheduleAdmin } from '../components/ScheduleAdmin'
import { ScheduleDay } from '../components/ScheduleDay'
import { CreateScheduleModal } from '../components/lessonModal/CreateScheduleModal'
import { EditScheduleModal } from '../components/lessonModal/EditScheduleModal'
import { ScheduleData } from '../store/ScheduleStore'
import '../style.css'
import { MAIN_ROUTE } from '../utils/constants'
export interface DragLessonData {
  isDragging: boolean
  target: ScheduleData
}

export interface CreateClassModalData {
  show: boolean
  weekDay: number
  classNumber: number
}

export interface EditClassModalData extends Omit<ScheduleData, 'groupId'> {
  show: boolean
}

const Schedule: React.FC = observer(() => {
  const [showEditModal, setShowEditModal] = useState<EditClassModalData>({
    show: false,
    weekDay: 0,
    classNumber: 0,
    id: 0,
    lessonId: 0,
    teacherId: 0,
  })

  const [showCreateModal, setShowCreateModal] = useState<CreateClassModalData>({
    show: false,
    weekDay: 0,
    classNumber: 0,
  })

  const { schedule, teacher, lesson } = useContext(Context)
  const mappedSchedule: Map<number, ScheduleData[]> = new Map()
  const { id } = useParams()
  const [currentDragLesson, setCurrentDragLesson] = useState<DragLessonData>({
    isDragging: false,
    target: {
      id: 0,
      weekDay: 0,
      classNumber: 0,
      lessonId: 0,
      groupId: 0,
      teacherId: 0,
      createdAt: '',
      updatedAt: '',
    },
  })

  useEffect(() => {
    schedule.loadSchedules(Number(id))
  }, [id])

  if (!Number(id)) {
    return (
      <>
        <h2>404: wrong group number</h2>
        <a href={MAIN_ROUTE}>Return to main page</a>
      </>
    )
  }

  Array.from({ length: 7 }, (_, i) => i + 1).forEach((weekDay) =>
    mappedSchedule.set(
      weekDay,

      Array.from({ length: 6 }, (_, i) => i + 1).map((classNumber) => ({
        id: -1 * classNumber,
        weekDay,
        classNumber,
        lessonId: 0,
        groupId: 0,
        teacherId: 0,
      })),
    ),
  )
  schedule.schedule.forEach((obj: ScheduleData) => {
    // const weekDay = mappedSchedule.get(obj.weekDay)
    // if (!weekDay) {
    //   return
    // }
    mappedSchedule.get(obj.weekDay)?.splice(obj.classNumber - 1, 1, obj)
    // mappedSchedule.set(obj.weekDay, weekDay)
  })

  const handleGearClick = (schedule: ScheduleData) => {
    console.log({ ...schedule })
    setShowEditModal({
      show: true,
      ...schedule,
    })
  }

  // console.log(mappedSchedule)

  const onCheckBoxChange = () => {
    setCurrentDragLesson((prev) => ({ ...prev, isDragging: !prev.isDragging }))
  }

  return (
    <Container fluid>
      <Row>
        <Col md={12} className="mb-5">
          <h1>{Number(id)} group schedule </h1>
          <a href={MAIN_ROUTE}>Return to main page</a>
          <div className="" style={{ cursor: 'pointer' }}>
            <Form.Check
              className="mt-5"
              checked={currentDragLesson.isDragging}
              style={{ cursor: 'pointer' }}
              type="switch"
              label="Edit schedule switch"
              id="edit-switch"
              onChange={onCheckBoxChange}
            />
          </div>
          <hr />
        </Col>
      </Row>
      <Row className="mb-5">
        {mappedSchedule.entries() &&
          Array.from(mappedSchedule.entries()).map(([weekDay, schedules]) => (
            <ScheduleDay weekDay={weekDay} key={weekDay}>
              <>
                {schedules.map((schedule) => (
                  <React.Fragment key={schedule.id}>
                    {schedule.id > 0 && (
                      <DraggableLesson
                        weekDay={weekDay}
                        currentDragLesson={currentDragLesson}
                        scheduleItem={schedule}
                        setCurrentDragLesson={setCurrentDragLesson}
                        mappedSchedule={mappedSchedule}
                      >
                        <LessonItem
                          scheduleItem={schedule}
                          handleGearClick={handleGearClick}
                        />
                      </DraggableLesson>
                    )}
                    {currentDragLesson.isDragging && schedule.id < 0 && (
                      <DraggableLesson
                        weekDay={weekDay}
                        currentDragLesson={currentDragLesson}
                        scheduleItem={schedule}
                        setCurrentDragLesson={setCurrentDragLesson}
                        mappedSchedule={mappedSchedule}
                      >
                        <PlaceholderLessonItem
                          setShowCreateModal={setShowCreateModal}
                          scheduleItem={schedule}
                        />
                      </DraggableLesson>
                    )}
                  </React.Fragment>
                ))}
                {schedules.filter((item) => item.id > 0).length < 6 &&
                  currentDragLesson.isDragging && (
                    <AddClassButton
                      setShowModal={setShowCreateModal}
                      weekDay={weekDay}
                      selectedGroup={Number(id)}
                    />
                  )}
              </>
            </ScheduleDay>
          ))}
        <ScheduleAdmin />
        {/* <ScheduleDay weekDay={7}>

      </ScheduleDay> */}

        {/*
        {Array.from(mappedSchedule.entries()).length < 7 &&
          Array.from({ length: 6 }, (_, i) => i + 1).map((weekDay) => {
            return (
              <ScheduleDay weekDay={weekDay} key={weekDay}>
                <AddClassButton weekDay={weekDay} selectedGroup={Number(id)} />
              </ScheduleDay>
            );
          })} */}
      </Row>
      {/* <EditClassModal
        selectedGroup={Number(id)}
        setShow={setShowEditModal}
        show={showEditModal.show}
        weekDay={showEditModal.weekDay}
        classNumber={showEditModal.classNumber}
        id={showEditModal.id}
        teacherId={showEditModal.teacherId}
        lessonId={showEditModal.lessonId}
      />
      {showCreateModal.show && (
        <ClassModal
          weekDay={showCreateModal.weekDay}
          teacher={teacher}
          lesson={lesson}
          classNumber={showCreateModal.classNumber}
          show={showCreateModal.show}
          setShow={setShowCreateModal}
          selectedGroup={Number(id)}
          schedule={schedule}
        />
      )} */}

      <CreateScheduleModal
        weekDay={showCreateModal.weekDay}
        classNumber={showCreateModal.classNumber}
        groupId={Number(id)}
        setShow={setShowCreateModal}
        show={showCreateModal.show}
      />
      <EditScheduleModal
        lessonId={showEditModal.lessonId}
        teacherId={showEditModal.teacherId}
        weekDay={showEditModal.weekDay}
        classNumber={showEditModal.classNumber}
        groupId={Number(id)}
        id={showEditModal.id}
        setShow={setShowEditModal}
        show={showEditModal.show}
      />
    </Container>
  )
})

export default Schedule
