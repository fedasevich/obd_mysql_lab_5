import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from "@nestjs/common";
import { TeacherDto } from "./dto/teacher.dto";
import { TeacherService } from "./teacher.service";

@Controller("teacher")
export class TeacherController {
  constructor(private teacherService: TeacherService) {}
  @Post()
  create(@Body() TeacherDto: TeacherDto) {
    return this.teacherService.createTeacher(TeacherDto);
  }

  @Get("/:teacher")
  getByValue(@Param() params: TeacherDto) {
    return this.teacherService.getTeacherByValue(params);
  }

  @Get()
  getAll() {
    return this.teacherService.getAllTeachers();
  }

  @Put(":id")
  update(
    @Param("id") id: number,
    @Body() TeacherDto: Partial<Omit<TeacherDto, "fullName">>
  ) {
    return this.teacherService.updateTeacher(id, TeacherDto);
  }

  @Delete(":id")
  remove(@Param("id") id: number) {
    return this.teacherService.remove(id);
  }
}
