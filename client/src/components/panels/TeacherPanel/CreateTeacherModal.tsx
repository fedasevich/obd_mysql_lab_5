import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { Context } from '../../..'
import { TeacherData } from '../../../store/TeacherStore'
import { CreateModalProps } from '../PanelProvider'
import TeacherModal from './TeacherModal'

export const CreateTeacherModal: React.FC<CreateModalProps> = observer(
  ({ show, setShow }) => {
    const { teacher } = useContext(Context)

    const handleClose = () => {
      setShow(false)
    }

    const handleCommit = (formData: Omit<TeacherData, 'id'>) => {
      teacher.createTeacher({ ...formData }).then(() => handleClose())
    }

    return (
      <TeacherModal onSubmit={handleCommit} show={show} onClose={handleClose} />
    )
  },
)
