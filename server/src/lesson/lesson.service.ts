import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { LessonDto } from './dto/lesson.dto';
import { Lesson } from './lesson.model';

@Injectable()
export class LessonService {
    constructor(@InjectModel(Lesson) private lessonRepository: typeof Lesson) { }
    async createLesson(dto: LessonDto) {
        const candidate = await this.getLessonByValue(dto);
        if (candidate) {
            throw new HttpException({ message: 'Already exist' }, HttpStatus.BAD_REQUEST);
        }
        const lesson = await this.lessonRepository.create(dto);
        return lesson;
    }
    async getLessonByValue(dto: LessonDto) {
        const lesson = await this.lessonRepository.findOne({ where: { name: dto.name } });
        return lesson;
    }
    async getAllLessons() {
        const lessons = await this.lessonRepository.findAll();
        return lessons;
    }
    updateLesson(id: number, dto: Partial<LessonDto>) {
        if (!Object.entries(dto).length) {
            throw new HttpException(
              { message: "Wrong data" },
              HttpStatus.BAD_REQUEST
            );
          }
        return Lesson.update({ name: dto.name }, { where: { id } });
    }
    remove(id: number) {
        return Lesson.destroy({ where: { id } });
    }
}
