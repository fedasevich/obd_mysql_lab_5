import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { Context } from '../../..'
import { GroupData } from '../../../store/GroupStore'
import { CreateModalProps } from '../PanelProvider'
import GroupModal from './GroupModal'

export const CreateGroupModal: React.FC<CreateModalProps> = observer(
  ({ show, setShow }) => {
    const { group } = useContext(Context)

    const handleClose = () => {
      setShow(false)
    }

    const handleCommit = (formData: Omit<GroupData, 'id'>) => {
      if (!formData.number || !formData.studAmount) {
        return alert('All fields must be filled')
      }
      group
        .createGroup(formData.number, formData.studAmount)
        .then(() => handleClose())
    }

    return (
      <GroupModal onSubmit={handleCommit} show={show} onClose={handleClose} />
    )
  },
)
