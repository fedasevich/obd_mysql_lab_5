import { ScheduleData } from '../store/ScheduleStore'
import { $host } from './index'

export interface ProfessionData {
  id: number;
  profession: string;
  capacity: number;
}


export const fetchProfessions = async (): Promise<ProfessionData[]> => {
  const { data } = await $host.get(`profession/`)
  return data
}
