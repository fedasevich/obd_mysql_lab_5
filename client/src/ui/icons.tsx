
import { ReactComponent as Google } from './google.svg'
import { ReactComponent as GearSix } from './GearSixBackground.svg'
import { ReactComponent as Delete } from './Trash.svg'

export const GoogleIcon = () => {
    return (
        <Google />
    )
}

export const SettingsBackgroundIcon = () => {
    return (
        <GearSix/>
    )
}


export const DeleteIcon = () => {
    return (
        <Delete/>
    )
}
