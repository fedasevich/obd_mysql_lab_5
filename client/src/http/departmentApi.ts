import { $host } from '.'
import { DepartmentData } from '../store/DepartmentStore'

export const fetchDepartments = async (): Promise<DepartmentData[]> => {
  const { data } = await $host.get('department')
  return data
}
export const deleteDepartment = async (id: number) => {
  const { data } = await $host.delete(`department/${id}`)
  return data
}

export const createDepartment = async (
  params: Omit<DepartmentData, 'id'>,
): Promise<DepartmentData> => {
  const { data } = await $host.post('department/', { ...params })
  return data
}

export const changeDepartment = async (params: DepartmentData) => {
  const { data } = await $host.put(`department/${params.id}`, { ...params })
  return data
}
