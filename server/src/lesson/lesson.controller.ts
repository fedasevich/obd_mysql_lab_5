import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { LessonDto } from './dto/lesson.dto';
import { LessonService } from './lesson.service';

@Controller('lesson')
export class LessonController {
    constructor(private lessonService: LessonService) { }
    @Post()
    create(@Body() LessonDto: LessonDto) {
        return this.lessonService.createLesson(LessonDto);
    }


    @Get('/:lesson')
    getByValue(@Param() params: LessonDto) {
        return this.lessonService.getLessonByValue(params);
    }


    @Get()
    getAll() {
        return this.lessonService.getAllLessons();
    }


    @Put(':id')
    update(@Param('id') id: number, @Body() LessonDto: Partial<LessonDto>) {
        return this.lessonService.updateLesson(id, LessonDto);
    }


    @Delete(':id')
    remove(@Param('id') id: number) {
        return this.lessonService.remove(id);
    }
}
