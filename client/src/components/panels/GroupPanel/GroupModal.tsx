import { observer } from 'mobx-react-lite'
import React, { useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import { GroupData } from '../../../store/GroupStore'
import { PanelModalProps } from '../PanelProvider'

const GroupModal: React.FC<PanelModalProps<GroupData>> = observer(
  ({ show, onClose, onSubmit }) => {
    const [formData, setFormData] = useState<Omit<GroupData, 'id'>>({
      number: 0,
      studAmount: 0,
    })

    const handleCommit = () => {
      onSubmit(formData)
    }

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      setFormData((prev) => ({
        ...prev,
        [event.target.name]: event.target.valueAsNumber,
      }))
    }

    return (
      <Modal show={show} onHide={onClose}>
        <Modal.Header closeButton>
          <Modal.Title>Group modal </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="number">Enter new number:</Form.Label>
              <Form.Control
                id="number"
                type="number"
                name="number"
                placeholder=""
                value={formData.number || ''}
                onChange={handleInputChange}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="studAmount">
                Enter new amount of students:
              </Form.Label>
              <Form.Control
                id="studAmount"
                value={formData.studAmount || ''}
                type="number"
                name="studAmount"
                placeholder=""
                onChange={handleInputChange}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={onClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleCommit}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    )
  },
)

export default GroupModal
