import { observer } from 'mobx-react-lite'
import React from 'react'
import { Col, Row } from 'react-bootstrap'
import { CreateClassModalData } from '../pages/Schedule'
import { ScheduleData } from '../store/ScheduleStore'

interface PlaceholderLessonItemProps {
  scheduleItem: ScheduleData
  setShowCreateModal: React.Dispatch<React.SetStateAction<CreateClassModalData>>
}

export const PlaceholderLessonItem: React.FC<PlaceholderLessonItemProps> =
  observer(({ scheduleItem, setShowCreateModal }) => {
    const handleClick = () => {
      console.log(scheduleItem.weekDay, scheduleItem.classNumber)

      setShowCreateModal((prev) => ({
        ...prev,
        show: true,
        weekDay: scheduleItem.weekDay,
        classNumber: scheduleItem.classNumber,
      }))
    }

    return (
      // <CSSTransition
      //     in={true}
      //     timeout={300}
      //     classNames="fade"
      //     appear
      //     unmountOnExit
      // >
      <Row
        className="dragPlaceholderButton d-flex flex-row align-items-center justify-content-center weekDayItem position-relative"
        onClick={handleClick}
        style={{ cursor: 'pointer' }}
      >
        <Col
          md={1}
          className="weekDayClassNumber align-items-center d-flex justify-content-center px-0"
        >
          <p>{scheduleItem.classNumber}</p>
        </Col>
        <Col className="text-center">
          <h4>drop here</h4>
        </Col>
      </Row>
      // </CSSTransition>
    )
  })
