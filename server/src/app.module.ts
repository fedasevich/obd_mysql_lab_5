import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { APP_FILTER } from "@nestjs/core";
import { SequelizeModule } from "@nestjs/sequelize";
import { Department } from "./department/department.model";
import { DepartmentModule } from "./department/department.module";
import { AllExceptionsFilter } from "./filters/all-exceptions.filter";
import { Group } from "./group/group.model";
import { GroupModule } from "./group/group.module";
import { Lesson } from "./lesson/lesson.model";
import { LessonModule } from "./lesson/lesson.module";
import { Profession } from "./profession/profession.model";
import { ProfessionModule } from "./profession/profession.module";
import { Schedule } from "./schedule/schedule.model";
import { ScheduleModule } from "./schedule/schedule.module";
import { Teacher } from "./teacher/teacher.model";
import { TeacherModule } from "./teacher/teacher.module";

@Module({
  controllers: [],
  providers: [
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
  imports: [
    ConfigModule.forRoot({
      envFilePath: ".env",
    }),
    SequelizeModule.forRoot({
      dialect: "postgres",
      host: process.env.POSTGRES_HOST,
      port: Number(process.env.POSTGRES_PORT),
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB,
      models: [Lesson, Profession, Department, Schedule, Group, Teacher],
      autoLoadModels: true,
    }),

    ScheduleModule,
    LessonModule,
    ProfessionModule,
    DepartmentModule,
    GroupModule,
    TeacherModule,
  ],
})
export class AppModule {}
