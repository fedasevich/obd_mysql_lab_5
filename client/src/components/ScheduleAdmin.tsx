import React from 'react'
import { observer } from 'mobx-react-lite'
import { ScheduleDayContainer } from './ScheduleDay';
import { Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { DEPARTMENT_ROUTE, GROUP_ROUTE, LESSON_ROUTE, TEACHER_ROUTE } from '../utils/constants';


export const ScheduleAdmin: React.FC = observer(() => {
    const navigate = useNavigate()
    return (
        <ScheduleDayContainer>
            <h2>
                Admin panel
            </h2>
            <hr />
            <div className="text-center d-flex flex-column" style={{ gap: "20px" }}>
                <Button variant="danger" onClick={() => navigate(TEACHER_ROUTE)}>Teacher panel</Button>
                <Button onClick={() => navigate(DEPARTMENT_ROUTE)}>Department panel</Button>
                <Button variant="info" onClick={() => navigate(LESSON_ROUTE)}>Lesson panel</Button>
                <Button variant="success" onClick={() => navigate(GROUP_ROUTE)}>Group panel</Button>
            </div>
        </ScheduleDayContainer>
    );
})
