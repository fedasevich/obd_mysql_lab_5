import { IsNumber, IsOptional } from "class-validator";
export class UpdateScheduleDto {
    @IsNumber({}, { message: "Must be number" })
    @IsOptional()
    readonly weekDay: number;
    @IsNumber({}, { message: "Must be number" })
    @IsOptional()
    readonly classNumber: number;
    @IsNumber({}, { message: "Must be number" })
    @IsOptional()
    readonly lessonId: number;
    @IsNumber({}, { message: "Must be number" })
    @IsOptional()
    readonly groupId: number;
    @IsNumber({}, { message: "Must be number" })
    @IsOptional()
    readonly teacherId: number;
}
