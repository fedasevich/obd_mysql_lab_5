import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/sequelize";
import { Sequelize } from "sequelize-typescript";
import { GetGroupScheduleById } from "./dto/get-group-schedule-by-id.dto";
import { ScheduleDto } from "./dto/schedule.dto";
import { UpdateTwoSchedulesDto } from "./dto/update-two-schedules.dto";
import { Schedule } from "./schedule.model";

@Injectable()
export class ScheduleService {
  constructor(
    @InjectModel(Schedule) private scheduleRepository: typeof Schedule,
    private readonly sequelize: Sequelize
  ) {}

  async createSchedule(dto: ScheduleDto) {
    const candidate = await this.checkSchedule(dto);
    if (candidate) {
      throw new HttpException(
        { message: "Wrong data" },
        HttpStatus.BAD_REQUEST
      );
    }
    const anotherGroupCandidate = await this.scheduleRepository.findOne({
      where: {
        classNumber: dto.classNumber,
        weekDay: dto.weekDay,
        teacherId: dto.teacherId,
        lessonId: dto.lessonId,
      },
    });
    if (anotherGroupCandidate) {
      throw new HttpException(
        { message: "Already exists" },
        HttpStatus.BAD_REQUEST
      );
    }
    const schedule = await this.scheduleRepository.create(dto);
    return schedule;
  }

  async getScheduleByGroupId(dto: GetGroupScheduleById) {
    const schedule = await this.scheduleRepository.findAll({
      where: { ...dto },
    });
    if (!schedule) {
      throw new HttpException(
        { message: "Wrong data" },
        HttpStatus.BAD_REQUEST
      );
    }
    return schedule;
  }
  async getAllSchedules() {
    const schedules = await this.scheduleRepository.findAll();
    return schedules;
  }

  async checkSchedule(dto: ScheduleDto) {
    const candidate = await this.scheduleRepository.findOne({
      where: {
        classNumber: dto.classNumber,
        groupId: dto.groupId,
        weekDay: dto.weekDay,
      },
    });
    return candidate !== null;
  }

  async checkExactSchedule(dto: ScheduleDto) {
    const candidate = await this.scheduleRepository.findOne({
      where: { ...dto },
    });
    return candidate !== null;
  }

  async updateSchedule(id: number, dto: ScheduleDto) {
    const oldLesson = await this.scheduleRepository.findOne({
      where: {
        id,
      },
    });
    const existingLessonsOnThatDay = await this.scheduleRepository.findAll({
      where: {
        lessonId: dto.lessonId,
        teacherId: dto.teacherId,
        groupId: dto.groupId,
        weekDay: dto.weekDay,
      },
    });
    if (
      !(oldLesson.get().weekDay === dto.weekDay) &&
      existingLessonsOnThatDay.length === 2
    ) {
      throw new HttpException(
        { message: "Already exist two same lessons" },
        HttpStatus.BAD_REQUEST
      );
    }
    console.log(existingLessonsOnThatDay.length);
    const candidate = await this.checkSchedule(dto);
    if (candidate) {
      throw new HttpException(
        { message: "Wrong data" },
        HttpStatus.BAD_REQUEST
      );
    }
    const updatedSchedule = await this.scheduleRepository.update(
      { ...dto },
      { where: { id } }
    );
    return updatedSchedule[0];
  }

  async updateTwoSchedules(dto: UpdateTwoSchedulesDto) {
    const existingLessonsOnToDay = await this.scheduleRepository.findAll({
      where: {
        lessonId: dto.fromSchedule.lessonId,
        teacherId: dto.fromSchedule.teacherId,
        groupId: dto.fromSchedule.groupId,
        weekDay: dto.fromSchedule.weekDay,
      },
    });
    const existingLessonsOnFromDay = await this.scheduleRepository.findAll({
      where: {
        lessonId: dto.toSchedule.lessonId,
        teacherId: dto.toSchedule.teacherId,
        groupId: dto.toSchedule.groupId,
        weekDay: dto.toSchedule.weekDay,
      },
    });
    console.log(existingLessonsOnToDay.length, existingLessonsOnFromDay.length);
    if (
      (existingLessonsOnToDay.length === 2 ||
        existingLessonsOnFromDay.length === 2) &&
      !(
        existingLessonsOnToDay.length === 2 &&
        existingLessonsOnFromDay.length === 1
      ) &&
      !(
        existingLessonsOnToDay.length === 1 &&
        existingLessonsOnFromDay.length === 2
      )
    ) {
      console.log("test");
      throw new HttpException(
        { message: "Already exist two same lessons" },
        HttpStatus.BAD_REQUEST
      );
    }
    try {
      const result = await this.sequelize.transaction(
        async (SequelizeTransaction) => {
          const fromScheduleCandidate = await this.checkExactSchedule(
            dto.fromSchedule
          );
          const toScheduleCandidate = await this.checkExactSchedule(
            dto.toSchedule
          );
          console.log(fromScheduleCandidate, toScheduleCandidate);
          if (fromScheduleCandidate || toScheduleCandidate) {
            throw new HttpException(
              { message: "Wrong data" },
              HttpStatus.BAD_REQUEST
            );
          }

          const updatedFromSchedule = await Schedule.update(
            { ...dto.fromSchedule },
            {
              where: { id: dto.fromSchedule.id },
              transaction: SequelizeTransaction,
            }
          );
          const updatedToSchedule = await Schedule.update(
            { ...dto.toSchedule },
            {
              where: { id: dto.toSchedule.id },
              transaction: SequelizeTransaction,
            }
          );
          await SequelizeTransaction.commit();

          return updatedFromSchedule[0] && updatedToSchedule[0];
        }
      );
      return result;
    } catch (error) {
      return error;
    }
  }

  async remove(id: number) {
    const deletedSchedule = await Schedule.destroy({ where: { id } });
    return deletedSchedule;
  }
}
