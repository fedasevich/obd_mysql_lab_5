import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from "@nestjs/common";
import { GroupDto } from "./dto/group.dto";
import { GroupService } from "./group.service";

@Controller("group")
export class GroupController {
  constructor(private groupService: GroupService) {}
  @Post()
  create(@Body() GroupDto: GroupDto) {
    return this.groupService.createGroup(GroupDto);
  }

  @Get("/:group")
  getByValue(@Param() params: GroupDto) {
    return this.groupService.getGroupByValue(params);
  }

  @Get()
  getAll() {
    return this.groupService.getAllGroups();
  }

  @Put(":id")
  update(@Param("id") id: number, @Body() GroupDto: Partial<GroupDto>) {
    return this.groupService.updateGroup(id, GroupDto);
  }

  @Delete(":id")
  remove(@Param("id") id: number) {
    return this.groupService.remove(id);
  }
}
