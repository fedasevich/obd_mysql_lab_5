import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { Context } from '../..'
import { EditClassModalData } from '../../pages/Schedule'
import { ScheduleData } from '../../store/ScheduleStore'
import { ScheduleModal } from './ScheduleModal'

interface EditScheduleModalProps {
  classNumber: number
  weekDay: number
  show: boolean
  id: number
  groupId: number
  lessonId: number
  teacherId: number
  setShow: React.Dispatch<React.SetStateAction<EditClassModalData>>
}

export const EditScheduleModal: React.FC<EditScheduleModalProps> = observer(
  ({
    show,
    setShow,
    classNumber,
    weekDay,
    groupId,
    id,
    lessonId,
    teacherId,
  }) => {
    const { schedule } = useContext(Context)

    const handleClose = () => {
      setShow((prev) => ({
        ...prev,
        show: false,
      }))
    }

    const handleCommit = (formData: Omit<ScheduleData, 'id'>) => {
      schedule
        .changeSchedule({
          ...formData,
          ...(!formData.lessonId && { lessonId }),
          ...(!formData.teacherId && { teacherId }),
          groupId,
          id,
        })
        .then(() => handleClose())
    }

    return (
      <ScheduleModal
        classNumber={classNumber}
        weekDay={weekDay}
        groupId={groupId}
        onSubmit={handleCommit}
        show={show}
        onClose={handleClose}
      />
    )
  },
)
