import { IsNumber, IsString } from "class-validator";
export class ProfessionDto {
    @IsString({ message: "Must be string" })
    readonly profession: string;
    @IsNumber({}, { message: "Must be number" })
    readonly capacity: number;
}
