import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { Context } from '../../..'
import { GroupData } from '../../../store/GroupStore'
import { EditModalProps } from '../PanelProvider'
import GroupModal from './GroupModal'

export const EditGroupModal: React.FC<EditModalProps> = observer(
  ({ id, show, setShow }) => {
    const { group } = useContext(Context)

    const handleClose = () => {
      setShow({ show: false, id: 0 })
    }

    const handleCommit = (formData: Omit<GroupData, 'id'>) => {
      group
        .changeGroup(id, formData.number, formData.studAmount)
        .then(() => handleClose())
    }

    return (
      <GroupModal onSubmit={handleCommit} show={show} onClose={handleClose} />
    )
  },
)
