import { observer } from 'mobx-react-lite'
import React, { useContext, useState } from 'react'
import { Typeahead } from 'react-bootstrap-typeahead'
import { Option } from 'react-bootstrap-typeahead/types/types'
import { useNavigate } from 'react-router-dom'
import { Context } from '..'
import { ScheduleData } from '../store/ScheduleStore'
import { GroupData } from '../store/GroupStore'
import { SCHEDULE_ROUTE } from '../utils/constants'


const SelectGroup: React.FC = observer(() => {
  const { group } = useContext(Context)

  const navigate = useNavigate();

  const options = group.groups.map((group: GroupData) => {
    return group.id.toString()
  })

  const handleSelect = (selected: Option[]) => {
    navigate(`${SCHEDULE_ROUTE}/${selected[0]}`)
  };

  return (
    <div className="d-flex w-100 h-100 justify-content-center vh-100 align-items-center">
      <Typeahead
        style={{ width: "300px" }}
        id="basic-example"
        onChange={handleSelect}
        options={options}
        placeholder="Choose a group..."
      />
    </div>
  )
})

export default SelectGroup
