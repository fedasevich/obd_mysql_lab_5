import { getDay, isAfter, isBefore, parse } from 'date-fns'
import { LESSONS_TIMETABLE } from '../utils/constants'
import { StoreData } from './constants'

export const filterParams = <T extends StoreData>(params: T): T => {
  const result = Object.fromEntries(
    Object.entries(params).filter(([_, value]) => Boolean(value)),
  )
  return result as T
}

export const isCurrentTimeBetweenLessonTime = (
  classNumber: number,
  weekDay: number,
) => {
  const now = new Date()
  if (getDay(now) !== weekDay) {
    return false
  }
  const classNumberRange = LESSONS_TIMETABLE[classNumber - 1]
  const startTime = parse(classNumberRange.start, 'HH:mm', now)
  const endTime = parse(classNumberRange.end, 'HH:mm', now)
  return isAfter(now, startTime) && isBefore(now, endTime)
}
