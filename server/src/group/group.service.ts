import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/sequelize";
import { GroupDto } from "./dto/group.dto";

import { Group } from "./group.model";
@Injectable()
export class GroupService {
  constructor(
    @InjectModel(Group) private groupRepository: typeof Group
  ) { }
  async createGroup(dto: GroupDto) {
    const candidate = await this.groupRepository.findOne({
      where: { ...dto },
    });
    if (candidate) {
      throw new HttpException(
        { message: "Already exist" },
        HttpStatus.BAD_REQUEST
      );
    }
    const group = await this.groupRepository.create(dto);
    return group;
  }
  async getGroupByValue(dto: GroupDto) {
    const group = await this.groupRepository.findOne({
      where: { ...dto },
    });
    if (!group) {
      throw new HttpException(
        { message: "Wrong data" },
        HttpStatus.BAD_REQUEST
      );
    }
    return group;
  }
  async getAllGroups() {
    const groups = await this.groupRepository.findAll({
      order: [["number", "ASC"]],
    });
    return groups;
  }
  updateGroup(id: number, dto: Partial<GroupDto>) {
    if (!Object.entries(dto).length) {
      throw new HttpException(
        { message: "Wrong data" },
        HttpStatus.BAD_REQUEST
      );
    }
    return Group.update({ ...dto }, { where: { id } });
  }
  remove(id: number) {
    return Group.destroy({ where: { id } });
  }
}
