import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { Context } from '../../..'
import { LessonData } from '../../../store/LessonStore'
import { EditModalProps } from '../PanelProvider'
import { LessonModal } from './LessonModal'

export const EditLessonModal: React.FC<EditModalProps> = observer(
  ({ id, show, setShow }) => {
    const { lesson } = useContext(Context)

    const handleClose = () => {
      setShow({ show: false, id: 0 })
    }

    const handleCommit = (formData: Omit<LessonData, 'id'>) => {
      lesson.changeLesson({ id, ...formData }).then(() => handleClose())
    }

    return (
      <LessonModal onClose={handleClose} onSubmit={handleCommit} show={show} />
    )
  },
)
