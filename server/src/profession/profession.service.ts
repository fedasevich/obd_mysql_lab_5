
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { ProfessionDto } from './dto/profession.dto';
import { Profession } from './profession.model';
@Injectable()
export class ProfessionService {
    constructor(@InjectModel(Profession) private professionRepository: typeof Profession) { }
    async createProfession(dto: ProfessionDto) {
        const candidate = await this.getProfessionByValue(dto);
        if (candidate) {
            throw new HttpException({ message: 'Already exist' }, HttpStatus.BAD_REQUEST);
        }
        const profession = await this.professionRepository.create(dto);
        return profession;
    }
    async getProfessionByValue(dto: ProfessionDto) {
        const profession = await this.professionRepository.findOne({ where: { ...dto } });
        if (!profession) {
            throw new HttpException({ message: 'Wrong data' }, HttpStatus.BAD_REQUEST);
        }
        return profession;
    }
    async getAllProfessions() {
        const professions = await this.professionRepository.findAll();
        return professions;
    }
    updateProfession(id: number, dto: ProfessionDto) {
        return Profession.update({ ...dto }, { where: { id } });
    }
    remove(id: number) {
        return Profession.destroy({ where: { id } });
    }
}
