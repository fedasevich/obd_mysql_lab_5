import { IsNumber, IsNumberString } from "class-validator";
export class GetGroupScheduleById {
    @IsNumberString({}, { message: "Must be number" })
    readonly groupId: number;
}
