import { ScheduleData } from '../store/ScheduleStore'
import { $host } from './index'

export const fetchGroupSchedule = async (
  groupId: number,
): Promise<ScheduleData[]> => {
  const { data } = await $host.get(`schedule/${groupId}`)
  return data
}

export const createGroupSchedule = async (params: Omit<ScheduleData, 'id'>) => {
  try {
    const { data } = await $host.post(`schedule`, {
      ...params,
    })
    return data
  } catch (error: any) {
    alert(error.response.data.message)
  }
}
export const editGroupSchedule = async (params: ScheduleData) => {
  try {
    const { data } = await $host.put(`schedule/${params.id}`, {
      ...params,
    })
    return data
  } catch (error: any) {
    alert(error.response.data.message)
  }
}

export const editTwoGroupSchedules = async (
  fromSchedule: ScheduleData,
  toSchedule: ScheduleData,
) => {
  try {
    const { data } = await $host.put(`schedule`, {
      fromSchedule,
      toSchedule,
    })
    return data
  } catch (error: any) {
    alert(error.response.data.message)
  }
}

export const deleteSchedule = async (id: number) => {
  try {
    const { data } = await $host.delete(`schedule/${id}`)
    return data
  } catch (error: any) {
    alert(error.response.data.message)
  }
}

// call add_lessons(3, 1, 208, "Культурологія", "Осипов А.О.", @status);

// select case
// WHEN @status = 0 then "Success"
// WHEN @status = -1 then "Invalid class number or week day"
// WHEN @status = -2 then "Invalid lecturer name"
// WHEN @status = -3 then "Invalid discipline name"
// WHEN @status = -4 then "Invalid group number"
// else ""
// end as STATUS;
