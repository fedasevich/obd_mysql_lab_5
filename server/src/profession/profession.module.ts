import { Module } from '@nestjs/common';
import { ProfessionService } from './profession.service';
import { ProfessionController } from './profession.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Profession } from './profession.model';

@Module({
  providers: [ProfessionService],
  controllers: [ProfessionController],
  imports: [
    SequelizeModule.forFeature([Profession])
  ],
  exports: [ProfessionService]
})
export class ProfessionModule {}
