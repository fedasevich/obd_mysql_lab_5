import { observer } from 'mobx-react-lite'
import React from 'react'
import { Col, Container, Form, InputGroup, Row } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'
import { DeleteIcon, SettingsBackgroundIcon } from '../../ui/icons'
import { StoreData } from '../../utils/constants'

export interface CreateModalProps {
  show: boolean
  setShow: React.Dispatch<React.SetStateAction<boolean>>
}

export interface PanelModalProps<T extends StoreData> {
  show: boolean
  onSubmit: (formData: Omit<T, 'id'>) => void
  onClose: () => void
}

export interface EditModalProps {
  id: number
  show: boolean
  setShow: React.Dispatch<
    React.SetStateAction<{
      show: boolean
      id: number
    }>
  >
}

interface PanelProviderProps {
  children: React.ReactNode[] | React.ReactNode
}

type PanelComposition = React.FunctionComponent<PanelProviderProps> & {
  Container: React.FC<PanelContainerProps>
  Item: React.FC<PanelItemProps>
  Title: React.FC<PanelTitleProps>
  Search: React.FC<PanelSearchProps>
  Header: React.FC<PanelHeaderProps>
  CreateItem: React.FC<PanelCreateItemProps>
}

const PanelProvider: PanelComposition = ({ children }) => {
  return <div className="m-2">{children}</div>
}

interface PanelContainerProps {
  children: React.ReactNode[] | React.ReactNode
}

const PanelContainer: React.FC<PanelContainerProps> = ({ children }) => {
  return (
    <Container>
      <Row className="gx-3">{children}</Row>
    </Container>
  )
}

interface PanelTitleProps {
  title: string
}

const PanelTitle: React.FC<PanelTitleProps> = ({ title }) => {
  const navigate = useNavigate()

  const goBack = () => {
    navigate(-1)
  }
  return (
    <div className="">
      <h2>{title}</h2>
      <a href="#" onClick={goBack}>
        Return to main page
      </a>
    </div>
  )
}

interface PanelHeaderProps {
  children: React.ReactNode[] | React.ReactNode
}

const PanelHeader: React.FC<PanelHeaderProps> = ({ children }) => {
  return (
    <>
      <div className="d-flex justify-content-between">{children}</div>
      <hr />
    </>
  )
}

interface PanelSearchProps {
  search: number
  setSearch: React.Dispatch<React.SetStateAction<number>>
}

const PanelSearch: React.FC<PanelSearchProps> = ({ search, setSearch }) => {
  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearch(event.target.valueAsNumber)
  }

  return (
    <div className="align-self-center">
      <InputGroup className="mb-3">
        <Form.Control
          value={search || ''}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
            handleSearchChange(event)
          }
          type="number"
          placeholder="search"
          aria-label="search"
          aria-describedby="search"
        />
      </InputGroup>
    </div>
  )
}

interface PanelItemProps {
  item: StoreData
  handleGearClick: (id: number) => void
  handleDeleteClick: (id: number) => void
}

const PanelItem: React.FC<PanelItemProps> = observer(
  ({ item, handleGearClick, handleDeleteClick }) => {
    return (
      <Col md={3} className="mb-3">
        <div className="mb-3 panelItem position-relative text-break">
          {Object.entries(item)
            .filter(([key]) => key !== 'createdAt' && key !== 'updatedAt')
            .map(([key, value]) => (
              <div key={key}>
                <strong>
                  {key
                    .replace(/([A-Z])/g, ' $1')
                    .trim()
                    .split(' ')
                    .map((word, index) =>
                      index === 0
                        ? word.charAt(0).toUpperCase() + word.slice(1)
                        : word.charAt(0).toLowerCase() + word.slice(1),
                    )
                    .join(' ')}
                  :{' '}
                </strong>
                {value}
              </div>
            ))}
          <div className="position-absolute top-50 start-100 translate-middle p-2 ">
            <span className="gear" onClick={() => handleGearClick(item.id)}>
              <SettingsBackgroundIcon />
            </span>
            <span className="delete" onClick={() => handleDeleteClick(item.id)}>
              <DeleteIcon />
            </span>
          </div>
        </div>
      </Col>
    )
  },
)

interface PanelCreateItemProps {
  setShow: React.Dispatch<React.SetStateAction<boolean>>
}

const PanelCreateItem: React.FC<PanelCreateItemProps> = ({ setShow }) => {
  const handleClick = () => {
    setShow(true)
  }

  return (
    <Col md={3} className="mb-3">
      <div className="mb-3 panelItem d-flex align-items-center justify-content-center">
        <button className="addClassButton" onClick={handleClick}>
          +
        </button>
      </div>
    </Col>
  )
}

PanelProvider.Container = PanelContainer
PanelProvider.Item = PanelItem
PanelProvider.CreateItem = PanelCreateItem
PanelProvider.Title = PanelTitle
PanelProvider.Search = PanelSearch
PanelProvider.Header = PanelHeader
export default PanelProvider
