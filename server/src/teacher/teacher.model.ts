import {
  Column,
  DataType,
  ForeignKey,
  HasMany,
  Model,
  Table,
} from "sequelize-typescript";
import { Department } from "src/department/department.model";
import { Profession } from "src/profession/profession.model";
import { Schedule } from "src/schedule/schedule.model";

interface TeacherCreationAttrs {
  name: string;
  lastName: string;
  middleName: string;
  professionNumber: number;
  departmentNumber: number;
}

@Table({ tableName: "teacher" })
export class Teacher extends Model<Teacher, TeacherCreationAttrs> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;
  @Column({
    type: DataType.STRING,
  })
  name: string;
  @Column({
    type: DataType.STRING,
  })
  lastName: string;
  @Column({
    type: DataType.STRING,
  })
  middleName: string;

  @Column({
    get() {
      const firstName = this.getDataValue("name");
      const lastName = this.getDataValue("lastName");
      const middleName = this.getDataValue("middleName");
      const fullName = `${lastName} ${firstName[0]}.${middleName[0]}`;
      this.setDataValue("fullName", fullName);
      return fullName;
    },
  })
  fullName: string;

  @Column({ type: DataType.INTEGER })
  @ForeignKey(() => Department)
  departmentNumber: number;
  @Column({ type: DataType.INTEGER })
  @ForeignKey(() => Profession)
  professionNumber: number;

  @HasMany(() => Schedule)
  schedules: Schedule[];
}
