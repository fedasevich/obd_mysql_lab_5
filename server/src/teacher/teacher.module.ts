import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { TeacherController } from './teacher.controller';
import { TeacherService } from './teacher.service';
import { Teacher } from './teacher.model';

@Module({
  controllers: [TeacherController],
  providers: [TeacherService],
  imports: [
    SequelizeModule.forFeature([Teacher])
  ],
  exports: [TeacherService]
})
export class TeacherModule {}
