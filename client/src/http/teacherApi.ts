import { TeacherData } from '../store/TeacherStore'
import { $host } from './index'

export const fetchTeachers = async (): Promise<TeacherData[]> => {
  const { data } = await $host.get('teacher')
  return data
}

export const deleteTeacher = async (id: number) => {
  const { data } = await $host.delete(`teacher/${id}`)
  return data
}

export const createTeacher = async (
  params: Omit<TeacherData, 'id' | 'fullName'>,
): Promise<TeacherData> => {
  const { data } = await $host.post('teacher/', { ...params })
  return data
}

export const changeTeacher = async (params: Omit<TeacherData, 'fullName'>) => {
  const { data } = await $host.put(`teacher/${params.id}`, { ...params })
  return data
}
