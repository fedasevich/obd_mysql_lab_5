import { IsNumber } from "class-validator";
export class GroupDto {
  @IsNumber({}, { message: "Must be number" })
  readonly number: number;
  @IsNumber({}, { message: "Must be number" })
  readonly studAmount: number;
}
