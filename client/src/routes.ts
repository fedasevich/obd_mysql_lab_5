import MainPage from "./pages/MainPage"
import Schedule from "./pages/Schedule"
import { DepartmentPanel } from "./pages/panels/DepartmentPanel"
import { GroupPanel } from "./pages/panels/GroupPanel"
import { LessonPanel } from "./pages/panels/LessonPanel"
import { TeacherPanel } from "./pages/panels/TeacherPanel"
import { DEPARTMENT_ROUTE, GROUP_ROUTE, LESSON_ROUTE, MAIN_ROUTE, SCHEDULE_ROUTE, TEACHER_ROUTE } from "./utils/constants"


export const publicRoutes = [
    {
        path: MAIN_ROUTE,
        Component: MainPage
    },
    {
        path: `${SCHEDULE_ROUTE}/:id`,
        Component: Schedule
    },
    {
        path: TEACHER_ROUTE,
        Component: TeacherPanel
    },
    {
        path: DEPARTMENT_ROUTE,
        Component: DepartmentPanel
    },
    {
        path: LESSON_ROUTE,
        Component: LessonPanel
    },
    {
        path: GROUP_ROUTE,
        Component: GroupPanel
    },
]
