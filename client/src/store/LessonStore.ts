import { action, makeAutoObservable } from "mobx";
import { changeLesson, createLesson, deleteLesson, fetchLessons } from "../http/lessonApi";

export type LessonData = {
    id: number;
    name: string;
}


export default class LessonStore {
    _lessons: Array<LessonData>
    constructor() {
        this._lessons = []
        makeAutoObservable(this)
    }

    setLesson(lessons: LessonData[]) {
        this._lessons = lessons
    }

    get lessons() {
        return this._lessons
    }

    getLessonById(id:number) {
        return this._lessons.find(lesson => lesson.id === id)
    }

    getLessonIdByName(name:string) {
        const lesson = this._lessons.find(lesson => lesson.name === name)
        if (!lesson) {
            return 0
        }
        return lesson.id
    }

    loadLessons() {
        return fetchLessons().then(action((lessons: LessonData[]) => {
            this._lessons = lessons
        }))
    }

    deleteLesson(id:number) {
        return deleteLesson(id).then(action(() => {
            const scheduleIndex = this._lessons.findIndex((item) => item.id === id)
            this._lessons.splice(scheduleIndex, 1)
        }))
    }

    createLesson(params: Omit<LessonData, "id">) {
        return createLesson(params).then(action((lesson: LessonData) => {
            this._lessons.push(lesson)
        }))
    }

    findLessonById(lessonId: number) {
        return this._lessons.find((item) => item.id === lessonId)
    }

    changeLesson(params:LessonData) {
        return changeLesson(params).then(action(() => {
            const lesson = this.findLessonById(params.id)
            if (lesson) {
                Object.assign(lesson, { ...lesson, ...params })
            }
        }))
    }
}
