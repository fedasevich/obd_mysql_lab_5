
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { DepartmentDto } from './dto/department.dto';
import { Department } from './department.model';
@Injectable()
export class DepartmentService {
    constructor(@InjectModel(Department) private departmentRepository: typeof Department) { }
    async createDepartment(dto: DepartmentDto) {
        const candidate = await this.getDepartmentByValue(dto);
        if (candidate) {
            throw new HttpException({ message: 'Already exist' }, HttpStatus.BAD_REQUEST);
        }
        const department = await this.departmentRepository.create(dto);
        return department;
    }
    async getDepartmentByValue(dto: DepartmentDto) {
        const department = await this.departmentRepository.findOne({ where: { ...dto } });
        return department;
    }
    async getAllDepartments() {
        const departments = await this.departmentRepository.findAll();
        return departments;
    }
    updateDepartment(id: number, dto: Partial<DepartmentDto>) {
        if (!Object.entries(dto).length) {
            throw new HttpException(
              { message: "Wrong data" },
              HttpStatus.BAD_REQUEST
            );
          }
        return Department.update({ ...dto }, { where: { id } });
    }
    remove(id: number) {
        return Department.destroy({ where: { id } });
    }
}
