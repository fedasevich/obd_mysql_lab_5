import { Column, DataType, HasMany, Model, Table } from "sequelize-typescript";
import { Schedule } from "src/schedule/schedule.model";

interface GroupCreationAttrs {
  number: number;
  studAmount: number;
}

@Table({ tableName: "group" })
export class Group extends Model<Group, GroupCreationAttrs> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;
  @Column({ type: DataType.INTEGER, unique: true })
  number: number;
  @Column({ type: DataType.INTEGER })
  studAmount: number;
  @HasMany(() => Schedule)
  schedules: Schedule[];
}
