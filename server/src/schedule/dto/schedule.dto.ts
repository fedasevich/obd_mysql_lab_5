import { IsNumber, IsOptional } from "class-validator";
export class ScheduleDto {
    @IsNumber({}, { message: "Must be number" })
    @IsOptional()
    readonly id?: number;
    @IsNumber({}, { message: "Must be number" })
    readonly weekDay: number;
    @IsNumber({}, { message: "Must be number" })
    readonly classNumber: number;
    @IsNumber({}, { message: "Must be number" })
    readonly lessonId: number;
    @IsNumber({}, { message: "Must be number" })
    readonly groupId: number;
    @IsNumber({}, { message: "Must be number" })
    readonly teacherId: number;
}
