import { observer } from 'mobx-react-lite';
import React, { useContext, useEffect, useState } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Context } from '.';
import AppRouter from './components/AppRouter';


const App = observer(() => {
  const { group, teacher, lesson, department } = useContext(Context)
  const [isLoading, setIsLoading] = useState<boolean>(true)

  useEffect(() => {
    Promise.all([
      group.loadGroups(),
      teacher.loadTeachers(),
      lesson.loadLessons(),
      department.loadDepartments(),
    ]).then(() => setIsLoading(false))
  }, [])

  if (isLoading) {
    return <h2>Loading...</h2>
  }
  return (
    <React.StrictMode>
      <BrowserRouter>
        <AppRouter />
      </BrowserRouter>
    </React.StrictMode>
  );
})

export default App;
