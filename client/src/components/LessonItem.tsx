import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { Col, Row } from 'react-bootstrap'
import { Context } from '..'
import { ScheduleData } from '../store/ScheduleStore'
import { SettingsBackgroundIcon } from '../ui/icons'
import { isCurrentTimeBetweenLessonTime } from '../utils/helpers'

interface LessonItemProps {
  scheduleItem: ScheduleData
  handleGearClick: (schedule: ScheduleData) => void
}

export const LessonItem: React.FC<LessonItemProps> = observer(
  ({ scheduleItem, handleGearClick }) => {
    const { lesson, teacher } = useContext(Context)

    const handleClick = () => {
      handleGearClick(scheduleItem)
    }

    return (
      <Row
        className={`d-flex flex-row weekDayItem position-relative rounded-xl ${
          isCurrentTimeBetweenLessonTime(
            scheduleItem.classNumber,
            scheduleItem.weekDay,
          )
            ? 'bg-orange'
            : ''
        }`}
      >
        <Col
          md={1}
          className="weekDayClassNumber align-items-center d-flex justify-content-center px-0"
        >
          <p>{scheduleItem.classNumber}</p>
        </Col>
        <Col className="text-center py-2">
          <h5>
            {lesson.getLessonById(scheduleItem.lessonId)?.name}{' '}
            {scheduleItem.id}
          </h5>
          <p className="mb-0">
            {teacher.getTeacherById(scheduleItem.teacherId)?.fullName}
          </p>
          <span
            className="position-absolute top-0 start-100 translate-middle p-2 gear"
            onClick={handleClick}
          >
            <SettingsBackgroundIcon />
          </span>
        </Col>
      </Row>
    )
  },
)
