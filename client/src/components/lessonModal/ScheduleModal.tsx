import { observer } from 'mobx-react-lite'
import React, { useContext, useEffect, useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import { Typeahead } from 'react-bootstrap-typeahead'
import { Option } from 'react-bootstrap-typeahead/types/types'
import { Context } from '../..'
import { ScheduleData } from '../../store/ScheduleStore'
import { WeekDay } from '../WeekDay'
import { PanelModalProps } from '../panels/PanelProvider'

interface ScheduleModalProps extends PanelModalProps<ScheduleData> {
  classNumber: number
  weekDay: number
  groupId: number
}

export const ScheduleModal: React.FC<ScheduleModalProps> = observer(
  ({ show, onClose, onSubmit, classNumber, weekDay, groupId }) => {
    const { teacher, lesson } = useContext(Context)
    const [selectedTeacherId, setSelectedTeacherId] = useState<number>(0)
    const [selectedLessonId, setSelectedLessonId] = useState<number>(0)
    const [selectedClassNumber, setSelectedClassNumber] = useState<number>(1)

    const handleSelectTeacher = (selected: Option[]) => {
      setSelectedTeacherId(teacher.getTeacherIdByFullName(selected.toString()))
    }

    const handleSelectLesson = (selected: Option[]) => {
      setSelectedLessonId(lesson.getLessonIdByName(selected.toString()))
    }

    const handleSelectClassNumber = (selected: string) => {
      setSelectedClassNumber(Number(selected))
    }

    const lessonOptions = lesson.lessons.map((lesson) => {
      return lesson.name
    })

    const teacherOptions = teacher.teachers.map((teacher) => {
      return teacher.fullName
    })

    const handleCommit = () => {
      onSubmit({
        classNumber: selectedClassNumber,
        groupId,
        lessonId: selectedLessonId,
        teacherId: selectedTeacherId,
        weekDay,
      })
    }

    useEffect(() => {
      setSelectedClassNumber(classNumber)
    }, [classNumber])

    console.log(classNumber, selectedClassNumber)

    return (
      <Modal show={show} onHide={onClose}>
        <Modal.Header closeButton>
          <Modal.Title>
            <WeekDay weekDay={weekDay} /> schedule modal
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Label>Choose teacher:</Form.Label>
            <Typeahead
              id="teacherSelect"
              onChange={handleSelectTeacher}
              options={teacherOptions}
              placeholder="Choose a teacher..."
            />
            <Form.Label>Choose lesson:</Form.Label>
            <Typeahead
              id="classSelect"
              onChange={handleSelectLesson}
              options={lessonOptions}
              placeholder="Choose lesson..."
            />
            <Form.Group className="mb-3">
              <Form.Label>Class number select</Form.Label>
              <Form.Select
                defaultValue={classNumber || 1}
                onChange={(event) => {
                  handleSelectClassNumber(event.target.value)
                }}
              >
                {Array.from({ length: 6 }, (_, i) => i + 1).map(
                  (mapClassNumber) => {
                    return (
                      <option key={mapClassNumber}>{mapClassNumber}</option>
                    )
                  },
                )}
              </Form.Select>
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={onClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleCommit}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    )
  },
)
