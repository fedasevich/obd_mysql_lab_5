import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { Context } from '../../..'
import { LessonData } from '../../../store/LessonStore'
import { CreateModalProps } from '../PanelProvider'
import { LessonModal } from './LessonModal'

export const CreateLessonModal: React.FC<CreateModalProps> = observer(
  ({ show, setShow }) => {
    const { lesson } = useContext(Context)

    const handleClose = () => {
      setShow(false)
    }

    const handleCommit = (formData: Omit<LessonData, 'id'>) => {
      lesson.createLesson({ ...formData }).then(() => handleClose())
    }

    return (
      <LessonModal onClose={handleClose} onSubmit={handleCommit} show={show} />
    )
  },
)
