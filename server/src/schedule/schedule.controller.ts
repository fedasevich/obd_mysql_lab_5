import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from "@nestjs/common";
import { GetGroupScheduleById } from "./dto/get-group-schedule-by-id.dto";
import { ScheduleDto } from "./dto/schedule.dto";
import { UpdateTwoSchedulesDto } from "./dto/update-two-schedules.dto";
import { ScheduleService } from "./schedule.service";

@Controller("schedule")
export class ScheduleController {
  constructor(private scheduleService: ScheduleService) {}
  @Post()
  create(@Body() ScheduleDto: ScheduleDto) {
    return this.scheduleService.createSchedule(ScheduleDto);
  }

  @Get("/:groupId")
  getByGroupId(@Param() params: GetGroupScheduleById) {
    return this.scheduleService.getScheduleByGroupId(params);
  }

  @Get()
  getAll() {
    return this.scheduleService.getAllSchedules();
  }

  @Put()
  updateTwoSchedules(@Body() dto: UpdateTwoSchedulesDto) {
    console.log(dto);
    return this.scheduleService.updateTwoSchedules(dto);
  }

  @Put(":id")
  update(@Param("id") id: number, @Body() dto: ScheduleDto) {
    return this.scheduleService.updateSchedule(id, dto);
  }

  @Delete(":id")
  remove(@Param("id") id: number) {
    return this.scheduleService.remove(id);
  }
}
