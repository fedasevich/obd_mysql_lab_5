import { GroupData } from '../store/GroupStore';
import { $host } from './index';

export const fetchGroups = async (): Promise<GroupData[]> => {
    const { data } = await $host.get('group');
    return data;
};


export const deleteGroup = async (id: number) => {
    const { data } = await $host.delete(`group/${id}`);
    return data;
};

export const createGroup = async (number: number, studAmount: number): Promise<GroupData> => {
    const { data } = await $host.post("group/", { number, studAmount });
    return data;
};

export const changeGroup = async (id: number, number: number, studAmount: number) => {
    const { data } = await $host.put(`group/${id}`, {
        ...(number && { number }),
        ...(studAmount && { studAmount }),
    });
    return data;
};
