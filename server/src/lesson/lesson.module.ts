import { Module } from "@nestjs/common";
import { SequelizeModule } from "@nestjs/sequelize";
import { LessonController } from "./lesson.controller";
import { Lesson } from "./lesson.model";
import { LessonService } from "./lesson.service";

@Module({
  providers: [LessonService],
  controllers: [LessonController],
  imports: [SequelizeModule.forFeature([Lesson])],
  exports: [LessonService],
})
export class LessonModule {}
