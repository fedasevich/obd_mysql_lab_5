import React, { useContext, useState, useCallback, useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import PanelProvider from '../../components/panels/PanelProvider';
import { Context } from '../..';
import { CreateTeacherModal } from '../../components/panels/TeacherPanel/CreateTeacherModal';
import { EditTeacherModal } from '../../components/panels/TeacherPanel/EditTeacherModal';

export const TeacherPanel: React.FC = observer(() => {
    const { teacher, } = useContext(Context);

    const [search, setSearch] = useState<number>(0);
    const [createModalShow, setCreateModalShow] = useState<boolean>(false);
    const [editModal, setEditModal] = useState<{ show: boolean, id: number }>({ show: false, id: 0 });
    const filteredTeachers = useCallback(() => {
        return teacher.teachers.filter((item) =>
        (search
            ? item.id.toString().includes(search.toString())
            : true)
        );
    }, [teacher.teachers, search]);

    const handleGearClick = (id: number) => {
        setEditModal({ show: true, id })
    };

    const handleDeleteClick = (id: number) => {
        teacher.deleteTeacher(id)
    };


    return (
        <>
            <PanelProvider>
                <PanelProvider.Header>
                    <PanelProvider.Title title="Teacher panel" />
                    <PanelProvider.Search search={search} setSearch={setSearch} />
                </PanelProvider.Header>
                <PanelProvider.Container>
                    <PanelProvider.CreateItem setShow={setCreateModalShow} />
                    {filteredTeachers().map((item) => (
                        <PanelProvider.Item key={item.id} item={item} handleDeleteClick={handleDeleteClick} handleGearClick={handleGearClick} />
                    ))}
                </PanelProvider.Container>
            </PanelProvider>
            <CreateTeacherModal show={createModalShow} setShow={setCreateModalShow} />
            {editModal.show && <EditTeacherModal show={editModal.show} setShow={setEditModal} id={editModal.id} />}
        </>
    );
});
