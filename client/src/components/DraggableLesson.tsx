import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { Container } from 'react-bootstrap'
import { Context } from '..'
import { DragLessonData } from '../pages/Schedule'
import { ScheduleData } from '../store/ScheduleStore'

interface DraggableLessonProps {
  weekDay: number
  scheduleItem: ScheduleData
  children: React.ReactNode
  currentDragLesson: {
    isDragging: boolean
    target: ScheduleData
  }
  mappedSchedule: Map<number, ScheduleData[]>
  setCurrentDragLesson: React.Dispatch<React.SetStateAction<DragLessonData>>
}

export const DraggableLesson: React.FC<DraggableLessonProps> = observer(
  ({
    weekDay,
    scheduleItem,
    children,
    currentDragLesson,
    setCurrentDragLesson,
  }) => {
    const { schedule } = useContext(Context)

    const handleDragOver = (event: React.DragEvent<HTMLDivElement>) => {
      event.preventDefault()
      event.stopPropagation()
      if (!(event.currentTarget as Element).classList.contains('bg-green')) {
        const _ = (event.currentTarget as Element).classList.toggle('bg-green')
        console.count('draggable-lesson1')
      }
    }

    const handleDragLeave = (event: React.DragEvent<HTMLDivElement>) => {
      console.count('draggable-lesson')
      const _ = (event.currentTarget as Element).classList.toggle('bg-green')
    }

    const handleDragStart = () => {
      setCurrentDragLesson({ target: scheduleItem, isDragging: true })
      console.log(scheduleItem)
    }

    const handleDragEnd = (event: React.DragEvent<HTMLDivElement>) => {
      console.log(event)
      const _ = (event.currentTarget as Element).classList.toggle('bg-green')
      // setCurrentDragLesson({
      //     isDragging: false, target: {
      //         id: 0,
      //         weekDay: 0,
      //         classNumber: 0,
      //         lessonId: 0,
      //         groupId: 0,
      //         teacherId: 0,
      //         createdAt: "",
      //         updatedAt: ""
      //     }
      // })
    }

    const handleDrop = (
      event: React.DragEvent<HTMLDivElement>,
      weekDay: number,
      lesson: ScheduleData,
    ) => {
      event.preventDefault()
      event.stopPropagation()
      if (
        weekDay === currentDragLesson.target.weekDay &&
        lesson.classNumber === currentDragLesson.target.classNumber
      ) {
        return
      }

      if (currentDragLesson.target.id < 0) {
        return
      }
      if (lesson.id > 0) {
        const { weekDay: oldWeekDay, classNumber: oldClassNumber } =
          currentDragLesson.target
        schedule.changeTwoSchedules(
          {
            id: currentDragLesson.target.id,
            weekDay,
            classNumber: lesson.classNumber,
            lessonId: currentDragLesson.target.lessonId,
            groupId: currentDragLesson.target.groupId,
            teacherId: currentDragLesson.target.teacherId,
          },
          {
            id: lesson.id,
            weekDay: oldWeekDay,
            classNumber: oldClassNumber,
            lessonId: lesson.lessonId,
            groupId: lesson.groupId,
            teacherId: lesson.teacherId,
          },
        )
        // schedule.changeSchedule(weekDay, currentDragLesson.target.groupId, currentDragLesson.target.lessonId, currentDragLesson.target.teacherId, lesson.classNumber, currentDragLesson.target.id);
        // schedule.changeSchedule(oldWeekDay, lesson.groupId, lesson.lessonId, lesson.teacherId, oldClassNumber, lesson.id);
        return
      }
      console.log(currentDragLesson.target)
      // console.log(lesson.classNumber, lesson.weekDay, weekDay)
      console.log(scheduleItem)
      schedule.changeSchedule({
        weekDay,
        groupId: currentDragLesson.target.groupId,
        lessonId: currentDragLesson.target.lessonId,
        teacherId: currentDragLesson.target.teacherId,
        classNumber: lesson.classNumber,
        id: currentDragLesson.target.id,
      })
      // mappedSchedule.get(weekDay)?.filter((item) => (item.classNumber > lesson.classNumber + 1) && (item.classNumber - currentLesson.classNumber > 1))?.forEach((item) => {
      //     console.log(item)
      // })

      // // schedule.changeSchedule(weekDay, lesson.groupId, lesson.lessonId, lesson.teacherId, lesson.classNumber, lesson.id);

      // mappedSchedule.get(weekDay)?.forEach((item) => {
      //     // console.log(item.classNumber)
      //     if (item.classNumber !== currentLesson.classNumber) {
      //         return;
      //     }
      //     // console.log(item.classNumber, currentLesson.classNumber);

      //     schedule.changeSchedule(weekDay, item.groupId, item.lessonId, item.teacherId, item.classNumber + 1, item.id);
      // });

      // setCurrentLesson({
      //     id: 0,
      //     weekDay: 0,
      //     classNumber: 0,
      //     lessonId: 0,
      //     groupId: 0,
      //     teacherId: 0,
      //     createdAt: "",
      //     updatedAt: "",
      // });
      // setCurrentDragLesson({
      //     isDragging: false, target: {
      //         id: 0,
      //         weekDay: 0,
      //         classNumber: 0,
      //         lessonId: 0,
      //         groupId: 0,
      //         teacherId: 0,
      //         createdAt: "",
      //         updatedAt: ""
      //     }
      // })
    }
    return (
      <Container
        fluid
        className={`draggable-lesson d-flex flex-column rounded-xl my-3 
        `}
        draggable={true}
        onDragOver={(event: React.DragEvent<HTMLDivElement>) =>
          handleDragOver(event)
        }
        onDragLeave={(event: React.DragEvent<HTMLDivElement>) =>
          handleDragLeave(event)
        }
        onDragStart={() => handleDragStart()}
        onDragEnd={(event: React.DragEvent<HTMLDivElement>) =>
          handleDragEnd(event)
        }
        onDrop={(event: React.DragEvent<HTMLDivElement>) =>
          handleDrop(event, weekDay, scheduleItem)
        }
      >
        {children}
      </Container>
    )
  },
)
