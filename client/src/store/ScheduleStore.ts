import { action, makeAutoObservable } from 'mobx'
import {
  createGroupSchedule,
  deleteSchedule,
  editGroupSchedule,
  editTwoGroupSchedules,
  fetchGroupSchedule,
} from '../http/scheduleApi'
import { filterParams } from '../utils/helpers'

export type ScheduleData = {
  id: number
  weekDay: number
  classNumber: number
  lessonId: number
  groupId: number
  teacherId: number
  createdAt?: string
  updatedAt?: string
}

export default class ScheduleStore {
  _schedules: ScheduleData[]
  constructor() {
    this._schedules = [
      {
        id: 524,
        weekDay: 1,
        classNumber: 1,
        lessonId: 666,
        groupId: 107,
        teacherId: 50,
      },
    ]
    makeAutoObservable(this)
  }

  setSchedule(schedules: ScheduleData[]) {
    this._schedules = schedules
  }

  get schedule() {
    return this._schedules
  }

  getGroupScheduleFunc() {
    return this._schedules
  }

  sortSchedules() {
    this._schedules = this._schedules.sort((a, b) => {
      return a.weekDay - b.weekDay || a.classNumber - b.classNumber
    })
  }

  loadSchedules(groupId: number) {
    return fetchGroupSchedule(groupId).then(
      action((schedules: ScheduleData[]) => {
        this._schedules = schedules
        this.sortSchedules()
      }),
    )
  }

  createSchedule(params: Omit<ScheduleData, 'id'>) {
    return createGroupSchedule(params).then(
      action((newSchedule: ScheduleData) => {
        if (newSchedule) {
          this._schedules.push(newSchedule)
          this.sortSchedules()
        }
      }),
    )
  }

  findScheduleById(id: number) {
    return this._schedules.find((schedule) => schedule.id === id)
  }

  changeSchedule(params: ScheduleData) {
    console.log(params)
    const filteredParams = filterParams(params)
    return editGroupSchedule(filteredParams).then(
      action((data) => {
        if (!data) {
          return
        }
        const schedule = this.findScheduleById(params.id)
        if (!schedule) {
          return
        }
        Object.assign(schedule, {
          ...schedule,
          ...filteredParams,
        })
        this.sortSchedules()
      }),
    )
  }

  changeTwoSchedules(fromSchedule: ScheduleData, toSchedule: ScheduleData) {
    editTwoGroupSchedules(fromSchedule, toSchedule).then(
      action((data) => {
        console.log(data)
        if (!data) {
          return
        }
        const firstSchedule = this.findScheduleById(fromSchedule.id)
        const secondSchedule = this.findScheduleById(toSchedule.id)
        if (!firstSchedule || !secondSchedule) {
          return
        }
        Object.assign(firstSchedule, {
          lessonId: fromSchedule.lessonId,
          weekDay: fromSchedule.weekDay,
          classNumber: fromSchedule.classNumber,
          teacherId: fromSchedule.teacherId,
        })
        Object.assign(secondSchedule, {
          lessonId: toSchedule.lessonId,
          weekDay: toSchedule.weekDay,
          classNumber: toSchedule.classNumber,
          teacherId: toSchedule.teacherId,
        })
        this.sortSchedules()
      }),
    )
  }

  removeSchedule(id: number) {
    deleteSchedule(id).then(
      action(() => {
        const scheduleIndex = this._schedules.findIndex(
          (schedule) => schedule.id === id,
        )
        this._schedules.splice(scheduleIndex, 1)
      }),
    )
  }
}
