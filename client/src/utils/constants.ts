import { GroupData } from '../store/GroupStore'
import { LessonData } from '../store/LessonStore'
import { ScheduleData } from '../store/ScheduleStore'
import { TeacherData } from '../store/TeacherStore'

export const MAIN_ROUTE = '/main'
export const LOGIN_ROUTE = '/login'
export const SCHEDULE_ROUTE = '/schedule'
export const TEACHER_ROUTE = '/teacher'
export const DEPARTMENT_ROUTE = '/department'
export const LESSON_ROUTE = '/lesson'
export const GROUP_ROUTE = '/group'
export type StoreData =
  | GroupData
  | LessonData
  | ScheduleData
  | TeacherData
  | ScheduleData
export const LESSONS_TIMETABLE = [
  { start: '9:00', end: '10:20' },
  { start: '10:30', end: '11:50' },
  { start: '12:30', end: '13:50' },
  { start: '14:00', end: '15:20' },
  { start: '15:30', end: '16:50' },
  { start: '17:00', end: '18:20' },
]
