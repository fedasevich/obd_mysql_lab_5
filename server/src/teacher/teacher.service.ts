import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/sequelize";
import { TeacherDto } from "./dto/teacher.dto";

import { Teacher } from "./teacher.model";
@Injectable()
export class TeacherService {
  constructor(
    @InjectModel(Teacher) private teacherRepository: typeof Teacher
  ) {}
  async createTeacher(dto: TeacherDto) {
    const candidate = await this.getTeacherByValue(dto);
    if (candidate) {
      throw new HttpException(
        { message: "Already exist" },
        HttpStatus.BAD_REQUEST
      );
    }
    const teacher = await this.teacherRepository.create(dto);
    return teacher;
  }
  async getTeacherByValue(dto: TeacherDto) {
    const teacher = await this.teacherRepository.findOne({ where: { ...dto } });
    return teacher;
  }
  async getAllTeachers() {
    const teachers = await this.teacherRepository.findAll();
    return teachers;
  }
  async updateTeacher(id: number, dto: Partial<TeacherDto>) {
    if (!Object.entries(dto).length) {
      throw new HttpException(
        { message: "Wrong data" },
        HttpStatus.BAD_REQUEST
      );
    }
    const teacher = await this.teacherRepository.findOne({ where: { id } });
    return this.teacherRepository.update(
      {
        ...dto,
        ...(!dto.lastName && { lastName: teacher.lastName }),
        ...(!dto.middleName && { middleName: teacher.middleName }),
        ...(!dto.name && { name: teacher.name }),
      },
      { where: { id } }
    );
  }
  remove(id: number) {
    return Teacher.destroy({ where: { id } });
  }
}
