import { IsNumber, IsString } from "class-validator";
export class TeacherDto {
    @IsString({ message: "Must be string" })
    readonly name: string;
    @IsString({ message: "Must be string" })
    readonly lastName: string;
    @IsString({ message: "Must be string" })
    readonly middleName: string;
    @IsNumber({}, { message: "Must be number" })
    readonly professionNumber:number;
    @IsNumber({}, { message: "Must be number" })
    readonly departmentNumber:number;
}
