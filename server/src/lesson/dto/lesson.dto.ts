import { IsString } from "class-validator";
export class LessonDto {
    @IsString({ message: "Must be string" })
    readonly name: string;
}
