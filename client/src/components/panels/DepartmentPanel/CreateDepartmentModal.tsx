import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { Context } from '../../..'
import { DepartmentData } from '../../../store/DepartmentStore'
import { CreateModalProps } from '../PanelProvider'
import { DepartmentModal } from './DepartmentModal'

export const CreateDepartmentModal: React.FC<CreateModalProps> = observer(
  ({ show, setShow }) => {
    const { department } = useContext(Context)

    const handleClose = () => {
      setShow(false)
    }

    const handleCommit = (formData: Omit<DepartmentData, 'id'>) => {
      department.createDepartment({ ...formData }).then(() => handleClose())
    }

    return (
      <DepartmentModal
        onSubmit={handleCommit}
        show={show}
        onClose={handleClose}
      />
    )
  },
)
