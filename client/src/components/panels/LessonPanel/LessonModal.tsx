import { observer } from 'mobx-react-lite'
import React, { useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import { LessonData } from '../../../store/LessonStore'
import { PanelModalProps } from '../PanelProvider'

export const LessonModal: React.FC<PanelModalProps<LessonData>> = observer(
  ({ show, onClose, onSubmit }) => {
    const [formData, setFormData] = useState<Omit<LessonData, 'id'>>({
      name: '',
    })

    const handleCommit = () => {
      onSubmit(formData)
    }

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      setFormData((prev) => ({
        ...prev,
        [event.target.name]: event.target.value,
      }))
    }

    return (
      <Modal show={show} onHide={onClose}>
        <Modal.Header closeButton>
          <Modal.Title>Lesson modal</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="name">Enter new name:</Form.Label>
              <Form.Control
                id="text"
                type="name"
                name="name"
                placeholder=""
                value={formData.name}
                onChange={handleInputChange}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={onClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleCommit}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    )
  },
)
