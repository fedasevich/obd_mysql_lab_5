import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { Context } from '../../..'
import { DepartmentData } from '../../../store/DepartmentStore'
import { EditModalProps } from '../PanelProvider'
import { DepartmentModal } from './DepartmentModal'

export const EditDepartmentModal: React.FC<EditModalProps> = observer(
  ({ id, show, setShow }) => {
    const { department } = useContext(Context)

    const handleClose = () => {
      setShow({ show: false, id: 0 })
    }

    const handleCommit = (formData: Omit<DepartmentData, 'id'>) => {
      department.changeDepartment({ ...formData, id }).then(() => handleClose())
    }

    return (
      <DepartmentModal
        onSubmit={handleCommit}
        show={show}
        onClose={handleClose}
      />
    )
  },
)
