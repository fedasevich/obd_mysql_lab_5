import { observer } from 'mobx-react-lite'
import React from 'react'
import { CreateClassModalData } from '../pages/Schedule'

interface AddClassButtonProps {
  weekDay: number
  selectedGroup: number
  setShowModal: React.Dispatch<React.SetStateAction<CreateClassModalData>>
}

export const AddClassButton: React.FC<AddClassButtonProps> = observer(
  ({ weekDay, selectedGroup, setShowModal }) => {
    const handleShow = () => {
      setShowModal((prev) => ({ ...prev, show: true, weekDay, selectedGroup }))
    }
    return (
      <>
        <button className="addClassButton" onClick={handleShow}>
          +
        </button>
      </>
    )
  },
)
