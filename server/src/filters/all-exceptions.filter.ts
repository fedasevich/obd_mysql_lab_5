import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';
import { ValidationError } from 'sequelize';

type ResponseObject = {
  message: object,
}

@Catch(HttpException, ValidationError, Error) // add the ValidationError class to the decorator
export class AllExceptionsFilter implements ExceptionFilter {
  constructor(private readonly httpAdapterHost: HttpAdapterHost) { }

  catch(exception: HttpException | ValidationError, host: ArgumentsHost): void {
    const { httpAdapter } = this.httpAdapterHost;

    const ctx = host.switchToHttp();

    const httpStatus =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.BAD_REQUEST; // set the status code to 400 for validation errors

    const responseBody = {
      statusCode: httpStatus,
      timestamp: new Date().toISOString(),
      path: httpAdapter.getRequestUrl(ctx.getRequest()),
      message: exception instanceof HttpException
        ? (exception.getResponse() as ResponseObject).message
        : exception.message // use "exception.errors" for the validation errors
    };

    httpAdapter.reply(ctx.getResponse(), responseBody, httpStatus);
  }
}
