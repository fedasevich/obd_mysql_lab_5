import { action, makeAutoObservable } from 'mobx'
import {
  changeDepartment,
  createDepartment,
  deleteDepartment,
  fetchDepartments,
} from '../http/departmentApi'
import { filterParams } from '../utils/helpers'

export type DepartmentData = {
  id: number
  name: string
  phoneNumber: string
}

export default class DepartmentStore {
  _departments: Array<DepartmentData>
  constructor() {
    this._departments = []
    makeAutoObservable(this)
  }

  setDepartment(departments: DepartmentData[]) {
    this._departments = departments
  }

  get departments() {
    return this._departments
  }

  loadDepartments() {
    return fetchDepartments().then(
      action((departments: DepartmentData[]) => {
        this._departments = departments
      }),
    )
  }

  deleteDepartment(id: number) {
    return deleteDepartment(id).then(
      action(() => {
        const scheduleIndex = this._departments.findIndex(
          (item) => item.id === id,
        )
        this._departments.splice(scheduleIndex, 1)
      }),
    )
  }

  createDepartment(params: Omit<DepartmentData, 'id'>) {
    return createDepartment(params).then(
      action((department: DepartmentData) => {
        this._departments.push(department)
      }),
    )
  }

  findDepartmentById(departmentId: number) {
    return this._departments.find((item) => item.id === departmentId)
  }

  changeDepartment(params: DepartmentData) {
    const filteredParams = filterParams(params)
    return changeDepartment(filteredParams).then(
      action(() => {
        const department = this.findDepartmentById(filteredParams.id)
        if (department) {
          Object.assign(department, { ...department, ...filteredParams })
        }
      }),
    )
  }
}
