import React, { useContext, useState, useCallback } from 'react';
import { observer } from 'mobx-react-lite';
import PanelProvider from '../../components/panels/PanelProvider';
import { Context } from '../..';
import { CreateLessonModal } from '../../components/panels/LessonPanel/CreateLessonModal';
import { EditLessonModal } from '../../components/panels/LessonPanel/EditLessonModal';

export const LessonPanel: React.FC = observer(() => {
  const { lesson } = useContext(Context);

  const [search, setSearch] = useState<number>(0);
  const [createModalShow, setCreateModalShow] = useState<boolean>(false);
  const [editModal, setEditModal] = useState<{ show: boolean, id: number }>({ show: false, id: 0 });
  const filteredLessons = useCallback(() => {
    return lesson.lessons.filter((item) =>
    (search
      ? item.id.toString().includes(search.toString())
      : true)
    );
  }, [lesson.lessons, search]);

  const handleGearClick = (id: number) => {
    setEditModal({ show: true, id })
  };

  const handleDeleteClick = (id: number) => {
    lesson.deleteLesson(id)
  };

  return (
    <>
      <PanelProvider>
        <PanelProvider.Header>
          <PanelProvider.Title title="Lesson panel" />
          <PanelProvider.Search search={search} setSearch={setSearch} />
        </PanelProvider.Header>
        <PanelProvider.Container>
          <PanelProvider.CreateItem setShow={setCreateModalShow}/>
          {filteredLessons().map((item) => (
            <PanelProvider.Item key={item.id} item={item} handleDeleteClick={handleDeleteClick} handleGearClick={handleGearClick} />
          ))}
        </PanelProvider.Container>
      </PanelProvider>
      <CreateLessonModal show={createModalShow} setShow={setCreateModalShow} />
      <EditLessonModal show={editModal.show} setShow={setEditModal} id={editModal.id} />
    </>
  );
});
