import { LessonData } from '../store/LessonStore'
import { $host } from './index'

export const fetchLessons = async (): Promise<LessonData[]> => {
  const { data } = await $host.get('lesson')
  return data
}


export const deleteLesson = async (id: number) => {
  const { data } = await $host.delete(`lesson/${id}`);
  return data;
};

export const createLesson = async (params: Omit<LessonData, "id">): Promise<LessonData> => {
  const { data } = await $host.post("lesson/", { ...params });
  return data;
};

export const changeLesson = async (params: LessonData) => {
  const { data } = await $host.put(`lesson/${params.id}`, {
    ...params
  });
  return data;
};
