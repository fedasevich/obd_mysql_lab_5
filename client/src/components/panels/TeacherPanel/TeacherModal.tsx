import { observer } from 'mobx-react-lite'
import React, { useContext, useEffect, useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import { Typeahead } from 'react-bootstrap-typeahead'
import { Option } from 'react-bootstrap-typeahead/types/types'
import { Context } from '../../..'
import { ProfessionData, fetchProfessions } from '../../../http/professionApi'
import { TeacherData } from '../../../store/TeacherStore'
import { PanelModalProps } from '../PanelProvider'

const TeacherModal: React.FC<PanelModalProps<TeacherData>> = observer(
  ({ show, onClose, onSubmit }) => {
    const { department } = useContext(Context)
    const [professions, setProfessions] = useState<ProfessionData[]>([])
    const [formData, setFormData] = useState<Omit<TeacherData, 'id'>>({
      departmentNumber: 0,
      lastName: '',
      middleName: '',
      name: '',
      fullName: '',
      professionNumber: 0,
    })

    const handleCommit = () => {
      onSubmit(formData)
    }
    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      setFormData((prev) => ({
        ...prev,
        [event.target.name]: event.target.value,
      }))
    }

    const handleProfessionSelect = (selected: Option[]) => {
      setFormData((prev) => ({
        ...prev,
        professionNumber:
          professions.find((item) => item.profession === selected[0])?.id || 0,
      }))
    }

    const handleDepartmentSelect = (selected: Option[]) => {
      setFormData((prev) => ({
        ...prev,
        departmentNumber:
          department.departments.find((item) => item.name === selected[0])
            ?.id || 0,
      }))
    }

    const professionsOptions = professions.map((profession) => {
      return profession.profession
    })

    const departmentsOptions = department.departments.map((department) => {
      return department.name
    })

    useEffect(() => {
      department.loadDepartments()
      fetchProfessions().then((data) => {
        setProfessions(data)
      })
    }, [])

    return (
      <Modal show={show} onHide={onClose}>
        <Modal.Header closeButton>
          <Modal.Title>Teacher modal</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="name">Enter new name:</Form.Label>
              <Form.Control
                type="text"
                name="name"
                value={formData.name}
                id="name"
                placeholder=""
                onChange={handleInputChange}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="middleName">
                Enter new middle name:
              </Form.Label>
              <Form.Control
                type="text"
                name="middleName"
                value={formData.middleName}
                id="middleName"
                placeholder=""
                onChange={handleInputChange}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="lastName">Enter new last name:</Form.Label>
              <Form.Control
                type="text"
                name="lastName"
                value={formData.lastName}
                id="lastName"
                placeholder=""
                onChange={handleInputChange}
              />
            </Form.Group>

            <Form.Label htmlFor="professionSelect">
              Choose profession:
            </Form.Label>
            <Typeahead
              id="professionSelect"
              onChange={handleProfessionSelect}
              options={professionsOptions}
              placeholder="Choose a profession..."
            />

            <Form.Label htmlFor="departmentSelect">
              Choose department:
            </Form.Label>
            <Typeahead
              id="departmentSelect"
              onChange={handleDepartmentSelect}
              options={departmentsOptions}
              placeholder="Choose a department..."
            />
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={onClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleCommit}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    )
  },
)

export default TeacherModal
