import React, { useContext, useState, useCallback, useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import PanelProvider from '../../components/panels/PanelProvider';
import { Context } from '../..';
import { CreateDepartmentModal } from '../../components/panels/DepartmentPanel/CreateDepartmentModal';
import { EditDepartmentModal } from '../../components/panels/DepartmentPanel/EditDepartmentModal';

export const DepartmentPanel: React.FC = observer(() => {
    const { department, } = useContext(Context);

    const [search, setSearch] = useState<number>(0);
    const [createModalShow, setCreateModalShow] = useState<boolean>(false);
    const [editModal, setEditModal] = useState<{ show: boolean, id: number }>({ show: false, id: 0 });
    const filteredDepartments = useCallback(() => {
        return department.departments.filter((item) =>
        (search
            ? item.id.toString().includes(search.toString())
            : true)
        );
    }, [department.departments, search]);

    const handleGearClick = (id: number) => {
        setEditModal({ show: true, id })
    };

    const handleDeleteClick = (id: number) => {
        department.deleteDepartment(id)
    };


    return (
        <>
            <PanelProvider>
                <PanelProvider.Header>
                    <PanelProvider.Title title="Department panel" />
                    <PanelProvider.Search search={search} setSearch={setSearch} />
                </PanelProvider.Header>
                <PanelProvider.Container>
                    <PanelProvider.CreateItem setShow={setCreateModalShow} />
                    {filteredDepartments().map((item) => (
                        <PanelProvider.Item key={item.id} item={item} handleDeleteClick={handleDeleteClick} handleGearClick={handleGearClick} />
                    ))}
                </PanelProvider.Container>
            </PanelProvider>
            <CreateDepartmentModal show={createModalShow} setShow={setCreateModalShow} />
            {editModal.show && <EditDepartmentModal show={editModal.show} setShow={setEditModal} id={editModal.id} />}
        </>
    );
});
