import { observer } from 'mobx-react-lite'
import React, { HTMLAttributes } from 'react'
import { Col } from 'react-bootstrap'
import { WeekDay } from './WeekDay'

interface ScheduleDayProps extends HTMLAttributes<HTMLDivElement> {
  weekDay: number
  children: React.ReactNode[] | React.ReactNode
}

export const ScheduleDay: React.FC<ScheduleDayProps> = observer(({ weekDay, children, ...rest }) => {
  return (
    <ScheduleDayContainer {...rest}>
      <h2>
        <WeekDay weekDay={weekDay} />
      </h2>
      <hr />
      {children}
    </ScheduleDayContainer>
  )
}
)


interface ScheduleDayContainerProps extends HTMLAttributes<HTMLDivElement> {
  children: React.ReactNode[] | React.ReactNode
}

export const ScheduleDayContainer: React.FC<ScheduleDayContainerProps> = observer(({ children, ...rest }) => {
  return (
    <Col md={3} className="pb-5" {...rest}>
      <div className="weekDay">
        {children}
      </div>
    </Col>
  )
}
)

interface ScheduleDayContainerProps extends HTMLAttributes<HTMLDivElement> {
  children: React.ReactNode[] | React.ReactNode
}
