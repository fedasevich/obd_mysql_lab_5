import { action, makeAutoObservable } from 'mobx'
import {
  changeTeacher,
  createTeacher,
  deleteTeacher,
  fetchTeachers,
} from '../http/teacherApi'
import { filterParams } from '../utils/helpers'

export type TeacherData = {
  id: number
  name: string
  lastName: string
  middleName: string
  fullName: string
  professionNumber: number
  departmentNumber: number
}

export default class TeacherStore {
  _teachers: Array<TeacherData>
  constructor() {
    this._teachers = []
    makeAutoObservable(this)
  }

  setTeacher(teachers: TeacherData[]) {
    this._teachers = teachers
  }

  get teachers() {
    return this._teachers
  }

  getTeacherById(id: number) {
    return this._teachers.find((teacher) => teacher.id === id)
  }

  getTeacherIdByFullName(fullName: string) {
    const teacher = this._teachers.find(
      (teacher) => teacher.fullName === fullName,
    )
    if (!teacher) {
      return 0
    }
    return teacher.id
  }

  loadTeachers() {
    return fetchTeachers().then(
      action((teachers: TeacherData[]) => {
        this._teachers = teachers
      }),
    )
  }

  deleteTeacher(id: number) {
    return deleteTeacher(id).then(
      action(() => {
        const scheduleIndex = this._teachers.findIndex((item) => item.id === id)
        this._teachers.splice(scheduleIndex, 1)
      }),
    )
  }

  createTeacher(params: Omit<TeacherData, 'id' | 'fullName'>) {
    return createTeacher(params).then(
      action((teacher: TeacherData) => {
        this._teachers.push(teacher)
      }),
    )
  }

  findTeacherById(teacherId: number) {
    return this._teachers.find((item) => item.id === teacherId)
  }

  changeTeacher(params: Omit<TeacherData, 'fullName'>) {
    const filteredParams = filterParams(params)
    return changeTeacher(filteredParams).then(
      action(() => {
        const teacher = this.findTeacherById(filteredParams.id)
        if (teacher) {
          Object.assign(teacher, { ...teacher, ...filteredParams })
        }
      }),
    )
  }
}
