import { Module } from "@nestjs/common";
import { SequelizeModule } from "@nestjs/sequelize";
import { GroupController } from "./group.controller";
import { Group } from "./group.model";
import { GroupService } from "./group.service";

@Module({
  providers: [GroupService],
  controllers: [GroupController],
  imports: [SequelizeModule.forFeature([Group])],
  exports: [GroupService],
})
export class GroupModule {}
