import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { Context } from '../../..'
import { TeacherData } from '../../../store/TeacherStore'
import { EditModalProps } from '../PanelProvider'
import TeacherModal from './TeacherModal'

export const EditTeacherModal: React.FC<EditModalProps> = observer(
  ({ id, show, setShow }) => {
    const { teacher } = useContext(Context)

    const handleClose = () => {
      setShow({ show: false, id: 0 })
    }

    const handleCommit = (formData: Omit<TeacherData, 'id'>) => {
      teacher.changeTeacher({ ...formData, id }).then(() => handleClose())
    }

    return (
      <TeacherModal onSubmit={handleCommit} show={show} onClose={handleClose} />
    )
  },
)
