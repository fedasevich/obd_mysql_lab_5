
import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { DepartmentDto } from './dto/department.dto';
import { DepartmentService } from './department.service';
@Controller('department')
export class DepartmentController {
    constructor(private departmentService: DepartmentService) { }
    @Post()
    create(@Body() DepartmentDto: DepartmentDto) {
        return this.departmentService.createDepartment(DepartmentDto);
    }


    @Get('/:department')
    getByValue(@Param() params: DepartmentDto) {
        return this.departmentService.getDepartmentByValue(params);
    }


    @Get()
    getAll() {
        return this.departmentService.getAllDepartments();
    }


    @Put(':id')
    update(@Param('id') id: number, @Body() DepartmentDto: Partial<DepartmentDto>) {
        return this.departmentService.updateDepartment(id, DepartmentDto);
    }


    @Delete(':id')
    remove(@Param('id') id: number) {
        return this.departmentService.remove(id);
    }
}
