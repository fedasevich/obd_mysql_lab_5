import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { ProfessionDto } from './dto/profession.dto';
import { ProfessionService } from './profession.service';

@Controller('profession')
export class ProfessionController {
    constructor(private departmentService: ProfessionService) { }
    @Post()
    create(@Body() ProfessionDto: ProfessionDto) {
        return this.departmentService.createProfession(ProfessionDto);
    }


    @Get('/:department')
    getByValue(@Param() params: ProfessionDto) {
        return this.departmentService.getProfessionByValue(params);
    }


    @Get()
    getAll() {
        return this.departmentService.getAllProfessions();
    }


    @Put(':id')
    update(@Param('id') id: number, @Body() ProfessionDto: ProfessionDto) {
        return this.departmentService.updateProfession(id, ProfessionDto);
    }


    @Delete(':id')
    remove(@Param('id') id: number) {
        return this.departmentService.remove(id);
    }
}
