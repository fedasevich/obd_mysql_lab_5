import React from 'react'
import { observer } from 'mobx-react-lite'

interface WeekDayProps {
    weekDay: number
}

export const WeekDay: React.FC<WeekDayProps> = observer(({ weekDay }) => {
    const weekDays = [
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday',
    ]

    return <>{weekDays[weekDay - 1]}</>
})
