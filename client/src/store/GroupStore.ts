import { action, makeAutoObservable } from 'mobx'
import {
  changeGroup,
  createGroup,
  deleteGroup,
  fetchGroups,
} from '../http/groupApi'

export type GroupData = {
  id: number
  number: number
  studAmount: number
}

export default class GroupStore {
  _groups: Array<GroupData>
  constructor() {
    this._groups = []
    makeAutoObservable(this)
  }

  setGroup(groups: GroupData[]) {
    this._groups = groups
  }

  get groups() {
    return this._groups
  }

  loadGroups() {
    return fetchGroups().then(
      action((groups: GroupData[]) => {
        this._groups = groups
      }),
    )
  }

  deleteGroup(id: number) {
    return deleteGroup(id).then(
      action(() => {
        const scheduleIndex = this._groups.findIndex((item) => item.id === id)
        this._groups.splice(scheduleIndex, 1)
      }),
    )
  }

  createGroup(number: number, studAmount: number) {
    return createGroup(number, studAmount).then(
      action((group: GroupData) => {
        this._groups.push(group)
      }),
    )
  }

  findGroupById(groupId: number) {
    return this._groups.find((item) => item.id === groupId)
  }

  changeGroup(id: number, number: number, studAmount: number) {
    return changeGroup(id, number, studAmount).then(
      action(() => {
        const group = this.findGroupById(id)
        console.log(id, this.findGroupById(id), this._groups)
        if (!group) {
          return
        }
        if (number) {
          group.number = number
        }
        if (studAmount) {
          group.studAmount = studAmount
        }
      }),
    )
  }
}
